package sdp.biwi.rest;

import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import sdp.biwi.utils.StatsHistory;
import sdp.biwi.utils.StatMsg;
import sdp.biwi.utils.StatToGatewayMsg;


@Path("stat")
public class StatService {

    private static final Gson gson = new Gson();
    private static StatsHistory statsHistory;

    @POST
    @Path("store")
    @Produces("application/json")
    @Consumes("application/json")
    public Response storeStat(String statStr) {
        statsHistory = StatsHistory.getInstance();
        StatToGatewayMsg stat = gson.fromJson(statStr, StatToGatewayMsg.class);
        StatMsg sm = new StatMsg(stat.getValue(), stat.getTimestamp());
        statsHistory.addStat(sm);
        System.out.println("Stat " + stat.getValue() + " from ID " + stat.getNodeId() + ".");
        return Response.status(200).entity(gson.toJson("stat added")).build();
    }

}
