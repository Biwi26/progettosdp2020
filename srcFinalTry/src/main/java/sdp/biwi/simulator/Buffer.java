package sdp.biwi.simulator;

public interface Buffer {

    void addMeasurement(Measurement m);

}
