package sdp.biwi.simulator;

import sdp.biwi.utils.StatMsg;
import sdp.biwi.utils.DevMean;

import java.util.ArrayList;


public class SlidingBuffer implements Buffer {

    private ArrayList<StatMsg> measures;
    private static SlidingBuffer instance = null;

    private SlidingBuffer() {
        this.measures = new ArrayList<StatMsg>();
    }

    public static synchronized SlidingBuffer getInstance() {
        if(instance == null)
            instance = new SlidingBuffer();
        return instance;
    }

    public static synchronized void deleteInstance() {
        instance = null;
    }


    public synchronized void addMeasurement(Measurement m) {
        measures.add(new StatMsg(m.getValue(), m.getTimestamp()));
    }

    public StatMsg meanAndSlide() {
        if(measures.size() >= 12) {
            StatMsg slidingMean = DevMean.calculateMean(measures.subList(0, 12));
            for(int i=0; i<6; i++)
                measures.remove(0);
            return slidingMean;
        } else
            return null;
    }
}
