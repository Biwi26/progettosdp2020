package sdp.biwi.p2pnet;

import io.grpc.stub.StreamObserver;

import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;

import sdp.biwi.NodeEnteredServiceGrpc.*;
import sdp.biwi.PeerProtocol.*;
import sdp.biwi.StatsProtocol;
import sdp.biwi.utils.NodeInfo;
import sdp.biwi.utils.NodeList;

import static sdp.biwi.NodeMain.thisNode;
import static sdp.biwi.NodeMain.localNodeList;


public class NodeEnteredServiceImpl extends NodeEnteredServiceImplBase {


    @Override
    public void nodeEntered(NodeEnteredMessage req, StreamObserver<OkGotIt> responseObserver) {
        ManagedChannel nextCh;
        NodeEnteredServiceBlockingStub enterStub;
        NodeInfo receivedNode = new NodeInfo(req.getNodeId(), req.getNodeIp(), req.getNodePort());
        int receivedNodeId = receivedNode.getId();
        OkGotIt reply;

        localNodeList = NodeList.getInstance();
        System.out.println("ID " + req.getNodeId() + " entered");

        // if I'm not alone
        if(thisNode.getNodeId() != receivedNodeId) {
            // it's always my next, wait for token and pass it to him
            thisNode.setNextNode(receivedNode);
            localNodeList.addNode(receivedNode);
        }
        reply = OkGotIt.newBuilder().setOk(true).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();

//        // another node entered the network
//        if(receivedNode.getId() != thisNode.getNodeId()) { // new node
//
//            // if it's my next, link to him
//            if(thisNode.isMyNextNode(receivedNode) || thisNode.getNodeId() == thisNode.getNextNode().getId()) {
//                System.out.println("New next: " + receivedNode.getId());
//                thisNode.setNextNode(receivedNode);
//            }
//
//            // add node to local map and respond
//            localNodeList.addNode(receivedNode);
//            OkGotIt reply = OkGotIt.newBuilder().setOk(true).build();
//            responseObserver.onNext(reply);
//            responseObserver.onCompleted();
//
//            // forward message to my next
//            while(true) {
//                nextCh = ManagedChannelBuilder
//                    .forTarget(thisNode.getNextNode().getIp() + ":" + thisNode.getNextNode().getPort())
//                    .usePlaintext(true)
//                    .build();
//                enterStub = sdp.biwi.NodeEnteredServiceGrpc.newBlockingStub(nextCh);
//                try {
//                    enterStub.nodeEntered(req);
//                    break;
//                } catch(StatusRuntimeException e) {
//                    nextCh.shutdown();
//                    localNodeList.removeNode(thisNode.getNextNode().getId());
//                    thisNode.searchNewNextNode();
//                }
//            }
//            if(!nextCh.isShutdown())
//                nextCh.shutdown();
//        }
//
//        // I'm the one who entered, drop the message
//        else {
//            OkGotIt reply = OkGotIt.newBuilder().setOk(true).build();
//            responseObserver.onNext(reply);
//            responseObserver.onCompleted();
//        }
    }


}
