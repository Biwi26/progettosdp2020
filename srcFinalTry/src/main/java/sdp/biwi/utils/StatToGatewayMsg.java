package sdp.biwi.utils;


public class StatToGatewayMsg {

    private int nodeId;
    private double value;
    // private LocalDateTime timestamp;
    private long timestamp;

    public StatToGatewayMsg(int nodeId, double value, long timestamp) {
        this.nodeId = nodeId;
        this.value = value;
        this.timestamp = timestamp;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
