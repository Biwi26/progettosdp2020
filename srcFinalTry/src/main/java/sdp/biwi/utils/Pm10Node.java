package sdp.biwi.utils;

import java.util.ArrayList;
import java.lang.InterruptedException;

import static sdp.biwi.NodeMain.thisNode;

public class Pm10Node {

    private int nodeId;
    private String nodeIp;
    private int nodePort;
    private NodeInfo nextNode;
    private NodeInfo previousNode;
    private boolean nodeIsExiting;
    private boolean nodeSignedExit;
    private boolean nodeJustEntered;
    private static ArrayList<StatMsg> localStatsQueue;
    private boolean hasTheToken;

	public Pm10Node(int nodeId, String nodeIp, int nodePort) {
		this.nodeId = nodeId;
		this.nodeIp = nodeIp;
		this.nodePort = nodePort;
        this.nodeIsExiting = false;
        Pm10Node.localStatsQueue = new ArrayList<>();
        this.hasTheToken = false;
        this.nodeSignedExit = false;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeIp() {
		return nodeIp;
	}

	public void setNodeIp(String nodeIp) {
		this.nodeIp = nodeIp;
	}

	public int getNodePort() {
		return nodePort;
	}

	public void setNodePort(int nodePort) {
		this.nodePort = nodePort;
	}

	public NodeInfo getNextNode() {
		return nextNode;
	}

	public void setNextNode(NodeInfo nextNode) {
		this.nextNode = nextNode;
	}

    public NodeInfo getPreviousNode() {
        return previousNode;
    }

    public synchronized void setPreviousNode(NodeInfo previousNode) {
        this.previousNode = previousNode;
    }

    public synchronized boolean getNodeIsExiting() {
        return nodeIsExiting;
    }

    public synchronized void setNodeIsExiting() {
        this.nodeIsExiting = true;
    }

    public synchronized boolean getNodeSignedExit() {
        return nodeSignedExit;
    }

    public synchronized void signExit() {
        this.nodeSignedExit = true;
    }

    public synchronized void addStatToLocalQueue(StatMsg stat) {
        Pm10Node.localStatsQueue.add(stat);
    }

    public synchronized StatMsg popStatFromLocalQueue() {
        if(localStatsQueue.size() > 0)
            return Pm10Node.localStatsQueue.remove(0);
        else
            return null;
    }

    public synchronized void tokenIsMine() {
        this.hasTheToken = true;
    }

    public synchronized void tokenIsNotMine() {
        this.hasTheToken = false;
    }

    public synchronized boolean hasToken() {
        return hasTheToken;
    }

    public synchronized void waitTokenPassForExit() {
        while(!this.hasToken() && !this.getNodeSignedExit()) {
            try {
                wait();
            } catch(InterruptedException e) {
                System.out.println("Node interrupted while waiting for exit");
            }
        }
    }

    public synchronized void releaseTokenForExit() {
        if(this.getNodeSignedExit())
            notify();
    }

    public NodeInfo generateNodeInfo() {
        return new NodeInfo(this.nodeId, this.nodeIp, this.nodePort);
    }

    public synchronized void searchNewNextNode() {
        NodeInfo nodeFound = NodeList.getInstance().searchNextNode(this.nodeId);
        this.nextNode = (nodeFound!=null ? nodeFound : this.generateNodeInfo());
    }
    public synchronized void searchNewPreviousNode() {
        NodeInfo nodeFound = NodeList.getInstance().searchPreviousNode(this.nodeId);
        this.previousNode = (nodeFound!=null ? nodeFound : this.generateNodeInfo());
    }

    public synchronized boolean isMyNextNode(NodeInfo ni) {
        NodeList nm = NodeList.getInstance();
        int idUnderTest = ni.getId();
        int maxNodeId = nm.getMaxNodeId();
        int minNodeId = nm.getMinNodeId();
        // special case: I'm the highest and ni is the new highest / lowest
        if(this.nodeId == maxNodeId) {
            if(idUnderTest > maxNodeId || idUnderTest < minNodeId)
                return true;
        }
        // otherwise, x is my next node if thisNode < x < myNextNode
        if(this.nodeId < idUnderTest && idUnderTest < this.nextNode.getId()) {
            return true;
        }
        return false;
    }

}
