package sdp.biwi.utils;

import java.util.ArrayList;

public class NodeList {

    private ArrayList<NodeInfo> nodes;
    private static NodeList instance = null;

    private NodeList() {
        nodes = new ArrayList<NodeInfo>();
    }

    public static synchronized NodeList getInstance() {
        if(instance == null)
            instance = new NodeList();
        return instance;
    }

    public static synchronized void deleteInstance() {
        instance = null;
    }

    public synchronized boolean addNode(NodeInfo nodeInfo) {
        int nodeId = nodeInfo.getId();
        for(NodeInfo ni: nodes) {
            if(nodeId == ni.getId())
                return false;
        }
        nodes.add(nodeInfo);
        return true;
    }

    public synchronized boolean removeNode(int nodeId) {
        for(NodeInfo ni: nodes) {
            if(nodeId == ni.getId()) {
                nodes.remove(ni);
                return true;
            }
        }
        return false;
    }

    public synchronized int getNodesNumber() {
        return nodes.size();
    }

    public synchronized ArrayList<Integer> getNodeIds() {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(int i=0; i<instance.getNodesNumber(); i++) {
            ids.add(Integer.valueOf(nodes.get(i).getId()));
        }
        return ids;
    }

    public synchronized ArrayList<NodeInfo> getNodeInfoList() {
        return nodes;
    }

    public synchronized void importNodes(ArrayList<NodeInfo> incomingNodes) {
        instance.nodes = incomingNodes;
    }

    public NodeInfo getNodeById(int idToFind) {
        for(NodeInfo ni: nodes) {
            if(ni.getId() == idToFind)
                return ni;
        }
        return null;
    }

    public synchronized int getMaxNodeId() {
        int maxNode = Integer.MIN_VALUE;
        for(NodeInfo ni: nodes) {
            if(ni.getId() > maxNode) {
                maxNode = ni.getId();
            }
        }
        return maxNode;
    }

    public synchronized int getMinNodeId() {
        int minNode = Integer.MAX_VALUE;
        for(NodeInfo ni: nodes) {
            if(ni.getId() < minNode) {
                minNode = ni.getId();
            }
        }
        return minNode;
    }

    public synchronized NodeInfo searchNextNode(int currentNodeId) {
        if(nodes.size() > 1) {
            int nodeToDeliverIndex = -1;                // node idx to return
            int currentNextId = Integer.MAX_VALUE;      // next id to current
            int absoluteMinIdIndex = -1;                // min id and index to wrap ring
            int absoluteMinId = Integer.MAX_VALUE;      // if I'm the highest id
            int currentId;
            for(int i=0; i<nodes.size(); i++) {
                currentId = nodes.get(i).getId();
                if(currentId < currentNextId && currentId > currentNodeId) {
                    nodeToDeliverIndex = i;
                    currentNextId = currentId;
                }
                if(currentId < absoluteMinId) {
                    absoluteMinId = currentId;
                    absoluteMinIdIndex = i;
                }
            }
            if(nodeToDeliverIndex == -1)                // if no next was found
                return nodes.get(absoluteMinIdIndex);   // I'll wrap around the ring
            return nodes.get(nodeToDeliverIndex);       // otherwise return closest next id
        }
        return null;                                    // I'm alone, I'm my next
    }

    public synchronized NodeInfo searchPreviousNode(int currentNodeId) {
        if(nodes.size() > 1) {
            int nodeToDeliverIndex = -1;
            int currentPreviousId = Integer.MIN_VALUE;
            int absoluteMaxIdIndex = -1;
            int absoluteMaxId = Integer.MIN_VALUE;
            int currentId;
            for(int i=0; i<nodes.size(); i++) {
                currentId = nodes.get(i).getId();
                if(currentId > currentPreviousId && currentId < currentNodeId) {
                    nodeToDeliverIndex = i;
                    currentPreviousId = currentId;
                }
                if(currentId < absoluteMaxId) {
                    absoluteMaxId = currentId;
                    absoluteMaxIdIndex = i;
                }
            }
            if(nodeToDeliverIndex == -1)                // if no next was found
                return nodes.get(absoluteMaxIdIndex);   // I'll wrap around the ring
            return nodes.get(nodeToDeliverIndex);       // otherwise return closest next id
        }
        return null;                                    // I'm alone, I'm my next
    }

    public void printNodeMap() {
        for(NodeInfo ni: nodes)
            System.out.println("ID: " + ni.getId() + "\tIP: " + ni.getIp() + "\tport: " + ni.getPort());
    }

}
