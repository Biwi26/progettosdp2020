package sdp.biwi.utils;

import com.google.gson.Gson;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import java.io.IOException;

import static sdp.biwi.GatewayMain.SERVER_BASE_URI;
import static sdp.biwi.NodeMain.thisNode;

public class ExitListener implements Runnable {

    public ExitListener() {

    }

    public void run() {
        WebTarget target;
        Response response;
        Gson gson = new Gson();
        try {
            System.in.read();   // wait for ENTER to initiate exit procedure
        } catch(IOException e) {
            e.printStackTrace();
        }
        
    }

}
