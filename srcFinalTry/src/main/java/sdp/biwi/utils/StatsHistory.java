package sdp.biwi.utils;

import java.util.List;
import java.util.ArrayList;

public class StatsHistory {

    private ArrayList<StatMsg> stats;
    private static StatsHistory instance = null;

    private StatsHistory() {
        stats = new ArrayList<StatMsg>();
    }

    public static synchronized StatsHistory getInstance() {
        if(instance == null)
            instance = new StatsHistory();
        return instance;
    }

    public static synchronized void deleteInstance() {
        instance = null;
    }

    public synchronized void addStat(StatMsg stat) {
        stats.add(0, stat);
    }

    public synchronized void removeStat(StatMsg stat) {
        stats.remove(stat);
    }

    public synchronized StatMsg readStat(int index) {
        return stats.get(index);
    }

    public synchronized List<StatMsg> readNStats(int amount) {
        if(amount > stats.size())
            return stats;
        else
            return stats.subList(0, amount);
    }


}
