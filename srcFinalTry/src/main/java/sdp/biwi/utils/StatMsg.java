package sdp.biwi.utils;

import java.time.LocalDateTime;

public class StatMsg {

    private double value;
    // private LocalDateTime timestamp;
    private long timestamp;

    public StatMsg(double value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
