package sdp.biwi;


import com.google.gson.Gson;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;


import java.lang.InterruptedException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

import sdp.biwi.NodeEnteredServiceGrpc.*;
import sdp.biwi.TokenPassServiceGrpc.*;
import sdp.biwi.PeerProtocol.*;
import sdp.biwi.StatsProtocol.*;
import sdp.biwi.simulator.SlidingBuffer;
import sdp.biwi.simulator.PM10Simulator;
import sdp.biwi.utils.ExitListener;
import sdp.biwi.utils.NodeList;
import sdp.biwi.p2pnet.*;
import sdp.biwi.utils.Pm10Node;

import static sdp.biwi.GatewayMain.SERVER_BASE_URI;


public class NodeMain {

    public static Pm10Node thisNode;
    public static NodeList localNodeList;

    public static int fixNodeId(int wrongId) {
        System.out.println("Node ID " + wrongId + " already in use. Edit node ID: ");
        Integer input = null;
        boolean inputOk = false;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            try {
                input = Integer.parseInt(br.readLine());
                inputOk = true;
            } catch(NumberFormatException e) {
                System.out.print("Wrong input! ID must be int.");
                inputOk = false;
            } catch(IOException e) {
                System.out.println("I/O exception fixing node ID");
            }

        } while(!inputOk);
        return input;
    }

    public static void main(String[] args) {

        int inputId = Integer.parseInt(args[0]);
        int inputPort = Integer.parseInt(args[1]);
        String gatewayIp = args[2];
        int gatewayPort = Integer.parseInt(args[3]);

        WebTarget target;
        final Gson gson = new Gson();
        Server p2pServer;
        Response response;
        ManagedChannel nextCh;
        ManagedChannel prevCh;


        // contact gateway for registration
        target = ClientBuilder.newClient().target(SERVER_BASE_URI);
        System.out.println("Welcome, node " + inputId +
        ".\nYour port for p2p net is " + inputPort +
        ".\nContacting server " + gatewayIp + ":" + gatewayPort + "...");
        thisNode = new Pm10Node(inputId, "localhost", inputPort);
        response = target.path("node/register").request().post(Entity.json(gson.toJson(thisNode.generateNodeInfo())));

        // fix node ID if already in use
        if(response.getStatusInfo().getStatusCode() == 400) {
            boolean nodeIdValidation = false;
            do {
                inputId = fixNodeId(inputId);
                thisNode.setNodeId(inputId);
                response = target.path("node/register").request().post(Entity.json(gson.toJson(thisNode.generateNodeInfo())));
                if(response.getStatusInfo().getStatusCode() == 200) {
                    nodeIdValidation = true;
                }
            } while(!nodeIdValidation);
        }

        //response should contain node map at this point, print it
        System.out.println("Entered the network with ID " + thisNode.getNodeId() + ".");
        localNodeList = NodeList.getInstance();
        NodeList gatewayNodeList = gson.fromJson(response.readEntity(String.class), NodeList.class);
        localNodeList.importNodes(gatewayNodeList.getNodeInfoList());
        localNodeList.printNodeMap();

        // link to next node
        thisNode.searchNewNextNode();
        System.out.println("NODE " + thisNode.getNodeId() + ": My next is: " + thisNode.getNextNode().getId());

        // start grpc service
        p2pServer = ServerBuilder
            .forPort(thisNode.getNodePort())
            .addService(new NodeEnteredServiceImpl())
            .addService(new TokenPassServiceImpl())
            .build();
        try {
            p2pServer.start();
            System.out.println("p2p server started.");
        } catch(IOException e) {
            System.out.println("Something went wrong starting p2p server.");
            e.printStackTrace();
        }

        // start exit listener
        Thread exitListener = new Thread(new ExitListener());
        exitListener.start();
        System.out.println("Press ENTER at any time to initiate exit procedure.");

//        Random rng = new Random();
//        try {
//            Thread.sleep(1000 + rng.nextInt(500));
//        } catch(InterruptedException e) {
//            e.printStackTrace();
//        }
        
        thisNode.searchNewPreviousNode();

        // enter ring contacting next node
        NodeEnteredMessage enterReq = NodeEnteredMessage.newBuilder()
            .setNodeId(thisNode.getNodeId())
            .setNodeIp(thisNode.getNodeIp())
            .setNodePort(thisNode.getNodePort())
            .build();
        prevCh = ManagedChannelBuilder
            .forTarget(thisNode.getNextNode().getIp() + ":" + thisNode.getPreviousNode().getPort())
            .usePlaintext(true)
            .build();

        NodeEnteredServiceBlockingStub nodeEnterStub = NodeEnteredServiceGrpc.newBlockingStub(prevCh);
        while(true) {
            try {
                nodeEnterStub.nodeEntered(enterReq);
                prevCh.shutdown();
                break;
            } catch(StatusRuntimeException e) {
                prevCh.shutdown();
                localNodeList.removeNode(thisNode.getNextNode().getId());
                thisNode.searchNewPreviousNode();
                nextCh = ManagedChannelBuilder
                    .forTarget(thisNode.getNextNode().getIp() + ":" + thisNode.getNextNode().getPort())
                    .usePlaintext(true)
                    .build();
                nodeEnterStub = NodeEnteredServiceGrpc.newBlockingStub(nextCh);
            }
        }
        // create token if I'm alone
        System.out.println("Nodes: " + gatewayNodeList.getNodesNumber());
        if(gatewayNodeList.getNodesNumber() == 1) {
            nextCh = ManagedChannelBuilder
                .forTarget(thisNode.getNextNode().getIp() + ":" + thisNode.getNextNode().getPort())
                .usePlaintext(true)
                .build();
            TokenPassServiceBlockingStub tokenPassStub = TokenPassServiceGrpc.newBlockingStub(nextCh);
            TokenMessage tokenReq = TokenMessage.newBuilder().build();
            tokenPassStub.passToken(tokenReq);
            System.out.println("Created new Token.");
            nextCh.shutdown();
        }

        // initialize simulator
        SlidingBuffer buffer = SlidingBuffer.getInstance();
        PM10Simulator pm10sim = new PM10Simulator("" + thisNode.getNodeId(), buffer);
        pm10sim.start();

        // wait for token to enter ring
        thisNode.waitForEntering();

        // wait for exitListener to trigger
        try {
            exitListener.join();
        } catch(InterruptedException e) {
            e.printStackTrace();
            System.out.println("Main was interrupted!");
        }

        // declare exit
        thisNode.setNodeIsExiting();

        // warn the server
        response = target.path("node/exit").request().post(Entity.json(gson.toJson(thisNode.getNodeId())));
        System.out.println("SERVER: " + gson.fromJson(response.readEntity(String.class), String.class));

        // wait until token is mine
        thisNode.waitTokenPassForExit();

        // shutdown & exit
        System.out.println("Main shutting down p2p server.");
        p2pServer.shutdown();
        System.exit(0);


    }
}
