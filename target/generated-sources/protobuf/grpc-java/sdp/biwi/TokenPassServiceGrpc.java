package sdp.biwi;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.7.0)",
    comments = "Source: StatsProtocol.proto")
public final class TokenPassServiceGrpc {

  private TokenPassServiceGrpc() {}

  public static final String SERVICE_NAME = "TokenPassService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<sdp.biwi.StatsProtocol.TokenMessage,
      sdp.biwi.StatsProtocol.TokenReceivedMessage> METHOD_PASS_TOKEN =
      io.grpc.MethodDescriptor.<sdp.biwi.StatsProtocol.TokenMessage, sdp.biwi.StatsProtocol.TokenReceivedMessage>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "TokenPassService", "passToken"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              sdp.biwi.StatsProtocol.TokenMessage.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              sdp.biwi.StatsProtocol.TokenReceivedMessage.getDefaultInstance()))
          .setSchemaDescriptor(new TokenPassServiceMethodDescriptorSupplier("passToken"))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static TokenPassServiceStub newStub(io.grpc.Channel channel) {
    return new TokenPassServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static TokenPassServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new TokenPassServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static TokenPassServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new TokenPassServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class TokenPassServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void passToken(sdp.biwi.StatsProtocol.TokenMessage request,
        io.grpc.stub.StreamObserver<sdp.biwi.StatsProtocol.TokenReceivedMessage> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_PASS_TOKEN, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_PASS_TOKEN,
            asyncUnaryCall(
              new MethodHandlers<
                sdp.biwi.StatsProtocol.TokenMessage,
                sdp.biwi.StatsProtocol.TokenReceivedMessage>(
                  this, METHODID_PASS_TOKEN)))
          .build();
    }
  }

  /**
   */
  public static final class TokenPassServiceStub extends io.grpc.stub.AbstractStub<TokenPassServiceStub> {
    private TokenPassServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TokenPassServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TokenPassServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TokenPassServiceStub(channel, callOptions);
    }

    /**
     */
    public void passToken(sdp.biwi.StatsProtocol.TokenMessage request,
        io.grpc.stub.StreamObserver<sdp.biwi.StatsProtocol.TokenReceivedMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_PASS_TOKEN, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class TokenPassServiceBlockingStub extends io.grpc.stub.AbstractStub<TokenPassServiceBlockingStub> {
    private TokenPassServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TokenPassServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TokenPassServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TokenPassServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public sdp.biwi.StatsProtocol.TokenReceivedMessage passToken(sdp.biwi.StatsProtocol.TokenMessage request) {
      return blockingUnaryCall(
          getChannel(), METHOD_PASS_TOKEN, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class TokenPassServiceFutureStub extends io.grpc.stub.AbstractStub<TokenPassServiceFutureStub> {
    private TokenPassServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TokenPassServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TokenPassServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TokenPassServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<sdp.biwi.StatsProtocol.TokenReceivedMessage> passToken(
        sdp.biwi.StatsProtocol.TokenMessage request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_PASS_TOKEN, getCallOptions()), request);
    }
  }

  private static final int METHODID_PASS_TOKEN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final TokenPassServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(TokenPassServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PASS_TOKEN:
          serviceImpl.passToken((sdp.biwi.StatsProtocol.TokenMessage) request,
              (io.grpc.stub.StreamObserver<sdp.biwi.StatsProtocol.TokenReceivedMessage>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class TokenPassServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    TokenPassServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return sdp.biwi.StatsProtocol.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("TokenPassService");
    }
  }

  private static final class TokenPassServiceFileDescriptorSupplier
      extends TokenPassServiceBaseDescriptorSupplier {
    TokenPassServiceFileDescriptorSupplier() {}
  }

  private static final class TokenPassServiceMethodDescriptorSupplier
      extends TokenPassServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    TokenPassServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (TokenPassServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new TokenPassServiceFileDescriptorSupplier())
              .addMethod(METHOD_PASS_TOKEN)
              .build();
        }
      }
    }
    return result;
  }
}
