package sdp.biwi;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.7.0)",
    comments = "Source: PeerProtocol.proto")
public final class NodeEnteredServiceGrpc {

  private NodeEnteredServiceGrpc() {}

  public static final String SERVICE_NAME = "NodeEnteredService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<sdp.biwi.PeerProtocol.NodeEnteredMessage,
      sdp.biwi.PeerProtocol.OkGotIt> METHOD_NODE_ENTERED =
      io.grpc.MethodDescriptor.<sdp.biwi.PeerProtocol.NodeEnteredMessage, sdp.biwi.PeerProtocol.OkGotIt>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "NodeEnteredService", "nodeEntered"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              sdp.biwi.PeerProtocol.NodeEnteredMessage.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              sdp.biwi.PeerProtocol.OkGotIt.getDefaultInstance()))
          .setSchemaDescriptor(new NodeEnteredServiceMethodDescriptorSupplier("nodeEntered"))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static NodeEnteredServiceStub newStub(io.grpc.Channel channel) {
    return new NodeEnteredServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static NodeEnteredServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new NodeEnteredServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static NodeEnteredServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new NodeEnteredServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class NodeEnteredServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void nodeEntered(sdp.biwi.PeerProtocol.NodeEnteredMessage request,
        io.grpc.stub.StreamObserver<sdp.biwi.PeerProtocol.OkGotIt> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_NODE_ENTERED, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_NODE_ENTERED,
            asyncUnaryCall(
              new MethodHandlers<
                sdp.biwi.PeerProtocol.NodeEnteredMessage,
                sdp.biwi.PeerProtocol.OkGotIt>(
                  this, METHODID_NODE_ENTERED)))
          .build();
    }
  }

  /**
   */
  public static final class NodeEnteredServiceStub extends io.grpc.stub.AbstractStub<NodeEnteredServiceStub> {
    private NodeEnteredServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private NodeEnteredServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NodeEnteredServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new NodeEnteredServiceStub(channel, callOptions);
    }

    /**
     */
    public void nodeEntered(sdp.biwi.PeerProtocol.NodeEnteredMessage request,
        io.grpc.stub.StreamObserver<sdp.biwi.PeerProtocol.OkGotIt> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_NODE_ENTERED, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class NodeEnteredServiceBlockingStub extends io.grpc.stub.AbstractStub<NodeEnteredServiceBlockingStub> {
    private NodeEnteredServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private NodeEnteredServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NodeEnteredServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new NodeEnteredServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public sdp.biwi.PeerProtocol.OkGotIt nodeEntered(sdp.biwi.PeerProtocol.NodeEnteredMessage request) {
      return blockingUnaryCall(
          getChannel(), METHOD_NODE_ENTERED, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class NodeEnteredServiceFutureStub extends io.grpc.stub.AbstractStub<NodeEnteredServiceFutureStub> {
    private NodeEnteredServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private NodeEnteredServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NodeEnteredServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new NodeEnteredServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<sdp.biwi.PeerProtocol.OkGotIt> nodeEntered(
        sdp.biwi.PeerProtocol.NodeEnteredMessage request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_NODE_ENTERED, getCallOptions()), request);
    }
  }

  private static final int METHODID_NODE_ENTERED = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final NodeEnteredServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(NodeEnteredServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_NODE_ENTERED:
          serviceImpl.nodeEntered((sdp.biwi.PeerProtocol.NodeEnteredMessage) request,
              (io.grpc.stub.StreamObserver<sdp.biwi.PeerProtocol.OkGotIt>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class NodeEnteredServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    NodeEnteredServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return sdp.biwi.PeerProtocol.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("NodeEnteredService");
    }
  }

  private static final class NodeEnteredServiceFileDescriptorSupplier
      extends NodeEnteredServiceBaseDescriptorSupplier {
    NodeEnteredServiceFileDescriptorSupplier() {}
  }

  private static final class NodeEnteredServiceMethodDescriptorSupplier
      extends NodeEnteredServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    NodeEnteredServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (NodeEnteredServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new NodeEnteredServiceFileDescriptorSupplier())
              .addMethod(METHOD_NODE_ENTERED)
              .build();
        }
      }
    }
    return result;
  }
}
