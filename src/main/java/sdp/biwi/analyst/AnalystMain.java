package sdp.biwi.analyst;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.lang.reflect.Type;


import sdp.biwi.utils.DevMean;
import sdp.biwi.utils.StatMsg;

import static sdp.biwi.gateway.GatewayMain.SERVER_BASE_URI;

public class AnalystMain {



    private static Scanner scanner = new Scanner(System.in);


    private static void printCommands() {
        System.out.println("\n0 - Exit the application");
        System.out.println("1 - See number of nodes currently active");
        System.out.println("2 - Get last N statistics of pollution");
        System.out.println("3 - Get mean and std. deviation of last N statistics of pollution");
        System.out.println("Type your command: ");
    }

    private static int readCorrectIntInput() {
        int input = -1;
        boolean inputOk;
        do {
            try {
                input = scanner.nextInt();
                inputOk = true;
            } catch(InputMismatchException e) {
                System.out.println("Wrong input! Retry. ");
                inputOk = false;
                scanner.nextLine();
            }

        } while(!inputOk);
        return input;
    }

    public static void main(String[] args) {
        int command = -1, input, sampleSize;
        boolean inputOk;
        Response response;
        WebTarget target;
        final Gson gson = new Gson();

        System.out.println("*** PM10 Pollution system controller ***");
        System.out.println("\nWelcome, analyst.\n");

        target = ClientBuilder.newClient().target(SERVER_BASE_URI).path("analyst");


        do {
            inputOk = false;
            printCommands();
            input = readCorrectIntInput();

            if(input <= 3 && input >= 0) {
                inputOk = true;
                command = input;
            }
            else {
                System.out.println("\nInvalid command.");
            }

            if(inputOk) {
                switch(command) {

                    case 0:
                        break;

                    case 1: // number of nodes in the network
                        response = target.path("nodes").request().get();
                        int nodesNum = gson.fromJson(response.readEntity(String.class), Integer.class);
                        System.out.println("Nodes currently active: " + nodesNum);
                        break;

                    case 2: // last N statistics
                        System.out.println("max amount of stats? ");
                        sampleSize = readCorrectIntInput();
                        response = target.path("stats").request().post(Entity.json(gson.toJson(sampleSize)));
                        // unmarshal and print stats
                        Type listType = new TypeToken<ArrayList<StatMsg>>(){}.getType();
                        ArrayList<StatMsg> statsToShow = gson.fromJson(response.readEntity(String.class), listType);
                        for(int i=0; i<statsToShow.size(); i++) {
                            LocalDateTime date = Instant.ofEpochMilli(statsToShow.get(i).getTimestamp()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                            System.out.println("PM10: " + statsToShow.get(i).getValue() + " -- " + date.toString());
                        }
                        break;

                    case 3: // std dev and mean of last N statistics
                        System.out.println("max amount of stats: ");
                        sampleSize = readCorrectIntInput();
                        response = target.path("dev-mean").request().post(Entity.json(gson.toJson(sampleSize)));
                        DevMean receivedDm = gson.fromJson(response.readEntity(String.class), DevMean.class);
                        LocalDateTime date = Instant.ofEpochMilli(receivedDm.getMean().getTimestamp()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                        System.out.println("Last " + sampleSize + " statistics");
                        System.out.println(" - Mean: " + receivedDm.getMean().getValue() + " -- " + date.toString());
                        System.out.println(" - Dev.: " + receivedDm.getStdDev());
                        break;
                }
            }

        } while(command != 0);

        System.out.println("Exiting the application ...");

    }

}
