package sdp.biwi.node;

import sdp.biwi.utils.DevMean;
import sdp.biwi.utils.NodeList;
import sdp.biwi.utils.StatMsg;

import java.lang.InterruptedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PM10Node {
    private int nodeId;
    private String nodeIp = "";
    private int nodePort;
    private boolean nodeIsExiting;
    private Object exitLock = new Object();
    private StatMsg localStat;
    private Object localStatLock = new Object();
    private boolean nodeSignedExit;
//    private Object signLock = new Object();
    private Object tokenLock = new Object();
    private ArrayList<NodeInfo> nodesEnteringQueue;


    public PM10Node(int nodeId, String nodeIp, int nodePort) {
        this.nodeId = nodeId;
        this.nodeIp = nodeIp;
        this.nodePort = nodePort;
        this.nodeIsExiting = false;
        this.localStat = null;
        this.nodeSignedExit = false;
        this.nodesEnteringQueue = new ArrayList<>();
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeIp() {
        return nodeIp;
    }

    public int getNodePort() {
        return nodePort;
    }


    public boolean getNodeIsExiting() {
        synchronized (exitLock) {
            return nodeIsExiting;
        }
    }

    public void setNodeIsExiting() {
        synchronized (exitLock) {
            this.nodeIsExiting = true;
        }
    }

    public StatMsg getLocalStat() {
        synchronized (localStatLock) {
            return this.localStat;
        }
    }

    public void clearLocalStat() {
        synchronized (localStatLock) {
            this.localStat = null;
        }
    }

    public void updateLocalStat(StatMsg stat) {
        synchronized (localStatLock) {
            if (localStat == null)
                localStat = stat;
            else
                localStat = DevMean.calculateMean(Arrays.asList(localStat, stat));
        }
    }

    public void waitTokenForExit() {
        synchronized (tokenLock) {
            while (!this.nodeSignedExit) {
                try {
                    tokenLock.wait();
                } catch (InterruptedException e) {
                    System.out.println("Node interrupted while waiting for exit");
                }
            }
        }
    }

    public void releaseTokenIfExit() {
        synchronized (tokenLock) {
            tokenLock.notifyAll();
        }
    }

    public void signExit() {
        synchronized (tokenLock) {
            this.nodeSignedExit = true;
        }
    }

    public boolean getNodeSignedExit() {
        synchronized (tokenLock) {
            return this.nodeSignedExit;
        }
    }



    public NodeInfo generateNodeInfo() {
        return new NodeInfo(this.nodeId, this.nodeIp, this.nodePort);
    }

    public NodeInfo searchNewNextNode(NodeList nodeList) {
        NodeInfo nextNode =nodeList.searchNextNode(this.nodeId);
        if(nextNode != null)
            return nextNode;
        else
            return generateNodeInfo();
    }

    public ArrayList<NodeInfo> emptyNodesEntering() {
        synchronized (nodesEnteringQueue) {
            ArrayList<NodeInfo> nodes = new ArrayList<>(nodesEnteringQueue);
            nodesEnteringQueue.clear();
            return nodes;
        }
    }

    public void addNodeEntering(NodeInfo newNode) {
        synchronized (nodesEnteringQueue) {
            nodesEnteringQueue.add(newNode);
        }
    }


}
