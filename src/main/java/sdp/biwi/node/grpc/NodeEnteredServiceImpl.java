package sdp.biwi.node.grpc;

import io.grpc.stub.StreamObserver;

import sdp.biwi.NodeEnteredServiceGrpc.*;
import sdp.biwi.PeerProtocol.*;
import sdp.biwi.node.NodeInfo;


import static sdp.biwi.node.NodeMain.thisNode;


public class NodeEnteredServiceImpl extends NodeEnteredServiceImplBase {


    @Override
    public void nodeEntered(NodeEnteredMessage req, StreamObserver<OkGotIt> responseObserver) {
        NodeInfo receivedNode = new NodeInfo(req.getNodeId(), req.getNodeIp(), req.getNodePort());
        OkGotIt reply;

        System.out.println("ID " + req.getNodeId() + " wants to enter.");

        if(!thisNode.getNodeIsExiting()) {
            reply = OkGotIt.newBuilder().setOk(true).build();
            if (receivedNode.getId() != thisNode.getNodeId()) {
                thisNode.addNodeEntering(receivedNode);
                System.out.println("Stored node " + req.getNodeId() + "'s enter request.");
            }
        } else
            reply = OkGotIt.newBuilder().setOk(false).build();

        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }


}
