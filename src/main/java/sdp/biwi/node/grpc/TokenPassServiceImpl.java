package sdp.biwi.node.grpc;

import com.google.gson.Gson;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import java.util.ArrayList;

import io.grpc.stub.StreamObserver;
import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannel;

import sdp.biwi.TokenPassServiceGrpc.*;
import sdp.biwi.StatsProtocol.*;
import sdp.biwi.node.NodeInfo;
import sdp.biwi.utils.NodeList;
import sdp.biwi.utils.StatMsg;
import sdp.biwi.utils.StatToGatewayMsg;
import sdp.biwi.simulator.SlidingBuffer;
import sdp.biwi.utils.DevMean;

import static sdp.biwi.gateway.GatewayMain.SERVER_BASE_URI;
import static sdp.biwi.node.NodeMain.thisNode;

public class TokenPassServiceImpl extends TokenPassServiceImplBase {

    @Override
    public void passToken(TokenMessage token, StreamObserver<TokenReceivedMessage> responseObserver) {

        ManagedChannel nextCh;
        TokenPassServiceBlockingStub tokenPassStub;
        TokenReceivedMessage reply;
        WebTarget target = ClientBuilder.newClient().target(SERVER_BASE_URI).path("stat");


        // respond to previous
        reply = TokenReceivedMessage.newBuilder().setReceived(true).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();

        ArrayList<NodeInfoMessage> tokenNodeList = new ArrayList<>(token.getNodesList());
        ArrayList<StatMessage> statsList = new ArrayList<>(token.getStatsList());


        if (thisNode.getNodeIsExiting()) {
            tokenNodeList.removeIf(ni -> ni.getNodeId() == thisNode.getNodeId());
            statsList.removeIf(sm -> sm.getNodeId() == thisNode.getNodeId());
            thisNode.signExit();
            token = TokenMessage.newBuilder()
                    .clearNodes()
                    .addAllNodes(tokenNodeList)
                    .clearStats()
                    .addAllStats(statsList)
                    .build();
        } else {
            // try to compute a stat
            StatMsg computedLocalStat = SlidingBuffer.getInstance().meanAndSlide();
            if (computedLocalStat != null)
                thisNode.updateLocalStat(computedLocalStat);

            // add stat if it's not in token yet and if I have one ready
            boolean statIsPresent = false;
            for (StatMessage sm : statsList) {
                if (sm.getNodeId() == thisNode.getNodeId()) {
                    statIsPresent = true;
                    break;
                }
            }
            StatMsg sampleFromQueue = null;
            StatMsg localStat = thisNode.getLocalStat();
            if (!statIsPresent && localStat != null) {
                sampleFromQueue = localStat;
                thisNode.clearLocalStat();
            }

            if (sampleFromQueue != null) {
                StatMessage statToAdd = StatMessage.newBuilder()
                        .setNodeId(thisNode.getNodeId())
                        .setValue(sampleFromQueue.getValue())
                        .setTimestamp(sampleFromQueue.getTimestamp())
                        .build();
                statsList.add(statToAdd);
            }

            // check if all stats are inside
            ArrayList<Integer> tokenIds = new ArrayList<>();
            for (NodeInfoMessage x : tokenNodeList)
                tokenIds.add(x.getNodeId());
            ArrayList<Integer> statsIds = new ArrayList<>();
            for (StatMessage sm : statsList)
                statsIds.add(sm.getNodeId());
            tokenIds.removeAll(statsIds);
            boolean readyToSend = tokenIds.size() == 0;


            if (readyToSend) {
                System.out.println("Token ready");
                // token is ready, calculate mean
                ArrayList<StatMsg> tokenReadableStats = new ArrayList<>();
                for (StatMessage tokenStat : statsList) {
                    tokenReadableStats.add(new StatMsg(tokenStat.getValue(), tokenStat.getTimestamp()));
                }
                StatMsg statToSend = DevMean.calculateMean(tokenReadableStats);
                System.out.println("stat mean: " + statToSend.getValue());
                // send stat to gateway
                final Gson gson = new Gson();
                Response response = target.path("store").request().post(Entity.json(gson.toJson(new StatToGatewayMsg(thisNode.getNodeId(), statToSend.getValue(), statToSend.getTimestamp()))));
                System.out.println("Gateway replies: " + response.getStatusInfo().getStatusCode());
                // clear token stats
                token = TokenMessage.newBuilder()
                        .clearNodes()
                        .addAllNodes(tokenNodeList)
                        .clearStats()
                        .build();
            } else {
                // token not ready yet, rebuild it with my stat added;
                token = TokenMessage.newBuilder()
                        .clearNodes()
                        .addAllNodes(tokenNodeList)
                        .addAllStats(statsList)
                        .build();
            }
        }

        // empty nodes entering list
        ArrayList<NodeInfo> nodesEntering = thisNode.emptyNodesEntering();
        for (NodeInfo ni : nodesEntering) {
            tokenNodeList.add(NodeInfoMessage.newBuilder()
                    .setNodeId(ni.getId())
                    .setNodeIp(ni.getIp())
                    .setNodePort((ni.getPort()))
                    .build());
        }
        if(nodesEntering.size() > 0) {
            token = TokenMessage.newBuilder()
                    .clearNodes()
                    .addAllNodes(tokenNodeList)
                    .clearStats()
                    .addAllStats(statsList)
                    .build();
        }

        // search new next to send token
        NodeList.deleteInstance();
        NodeList nodeList = NodeList.getInstance();
        ArrayList<NodeInfo> tokenNodes = new ArrayList<>();
        for(NodeInfoMessage nim: tokenNodeList)
            tokenNodes.add(new NodeInfo(nim.getNodeId(), nim.getNodeIp(), nim.getNodePort()));
        nodeList.importNodes(tokenNodes);
        NodeInfo targetNode = thisNode.searchNewNextNode(nodeList);

        // pass token
        if(!(thisNode.getNodeIsExiting()) || targetNode.getId() != thisNode.getNodeId()) {
            nextCh = ManagedChannelBuilder
                    .forTarget(targetNode.getIp() + ":" + targetNode.getPort())
                    .usePlaintext(true)
                    .build();
            tokenPassStub = sdp.biwi.TokenPassServiceGrpc.newBlockingStub(nextCh);
            tokenPassStub.passToken(token);
            System.out.println(thisNode.getNodeId() + ": token sent to " + targetNode.getId());
            nextCh.shutdown();
        }

        if(thisNode.getNodeSignedExit())
            thisNode.releaseTokenIfExit();

    }


}
