package sdp.biwi.node;

import com.google.gson.Gson;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.io.IOException;

public class ExitListener implements Runnable {

    public ExitListener() {

    }

    public void run() {
        try {
            System.in.read();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

}
