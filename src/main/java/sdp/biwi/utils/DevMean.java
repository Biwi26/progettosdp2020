package sdp.biwi.utils;

import java.lang.Math;
import java.util.List;

public class DevMean {

    private double stdDev;
    private StatMsg mean;


    public DevMean(double d, StatMsg m) {
        stdDev = d;
        mean = m;
    }

    public double getStdDev() {
        return stdDev;
    }
    public StatMsg getMean() {
        return mean;
    }

    public static double round(double value) {
        long factor = (long) Math.pow(10, 3);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    // calculates mean between values and median timestamp
    public static StatMsg calculateMean(List<StatMsg> stats) {
        double meanAcc = 0.0;
        long maxTimestamp = Long.MIN_VALUE;
        long minTimestamp = Long.MAX_VALUE;
        long currentTS;
        for(StatMsg currentStat: stats) {
            currentTS = currentStat.getTimestamp();
            meanAcc += currentStat.getValue();
            if(currentTS > maxTimestamp)
                maxTimestamp = currentTS;
            if(currentTS < minTimestamp)
                maxTimestamp = currentTS;
        }
        double mean = meanAcc / (double)stats.size();
        long medianTimestamp = minTimestamp + ((maxTimestamp - minTimestamp) / 2);
        return new StatMsg(round(mean), medianTimestamp);
    }

    // calculates std deviation of stats values
    public static double calculateStdDev(List<StatMsg> stats) {
        double mean = calculateMean(stats).getValue();
        double stdDevAcc = 0.0;
        for(StatMsg sm: stats)
            stdDevAcc += Math.pow(sm.getValue() - mean, 2);
        return round(Math.sqrt(stdDevAcc / stats.size()));
    }

}
