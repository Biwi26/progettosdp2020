package sdp.biwi.utils;

import java.time.LocalDateTime;

public class StatMsg {

    private double value;
    private long timestamp;

    public StatMsg(double value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

	public double getValue() {
		return value;
	}
	public long getTimestamp() {
		return timestamp;
	}

}
