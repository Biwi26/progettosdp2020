package sdp.biwi.utils;

import sdp.biwi.node.NodeInfo;

import java.util.ArrayList;

public class NodeList {

    private ArrayList<NodeInfo> nodes;
    private static NodeList instance = null;

    private NodeList() {
        nodes = new ArrayList<NodeInfo>();
    }

    public static synchronized NodeList getInstance() {
        if(instance == null)
            instance = new NodeList();
        return instance;
    }

    public static synchronized void deleteInstance() {
        instance = null;
    }

    public synchronized NodeList addNode(NodeInfo nodeInfo) {
        int nodeId = nodeInfo.getId();
        boolean nodeIsPresent = false;
        for(NodeInfo ni: nodes) {
            if(nodeId == ni.getId())
                nodeIsPresent = true;
        }
        if(!nodeIsPresent) {
            nodes.add(nodeInfo);
            return instance;
        } else
            return null;
    }

    public synchronized NodeList removeNode(int nodeId) {
        for(NodeInfo ni: nodes) {
            if(nodeId == ni.getId()) {
                nodes.remove(ni);
                return instance;
            }
        }
        return null;
    }

    public synchronized int getNodesNumber() {
        return nodes.size();
    }

    public synchronized ArrayList<Integer> getNodeIds() {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(int i=0; i<instance.getNodesNumber(); i++) {
            ids.add(nodes.get(i).getId());
        }
        return ids;
    }

    public synchronized ArrayList<NodeInfo> getNodeInfoList() {
        return nodes;
    }

    public synchronized void importNodes(ArrayList<NodeInfo> incomingNodes) {
        instance.nodes = incomingNodes;
    }

    public synchronized NodeInfo searchNextNode(int currentNodeId) {
        int nodeToDeliverIndex = -1;                // node idx to return
        int currentNextId = Integer.MAX_VALUE;      // next id to current
        int absoluteMinIdIndex = -1;                // min id and index to wrap ring
        int absoluteMinId = Integer.MAX_VALUE;      // if I'm the highest id
        int currentId;
        for(int i=0; i<nodes.size(); i++) {
            currentId = nodes.get(i).getId();
            if(currentId < currentNextId && currentId > currentNodeId) {
                nodeToDeliverIndex = i;
                currentNextId = currentId;
            }
            if(currentId < absoluteMinId) {
                absoluteMinId = currentId;
                absoluteMinIdIndex = i;
            }
        }
        if(nodeToDeliverIndex == -1 && absoluteMinIdIndex != -1)                // if no next was found
            return nodes.get(absoluteMinIdIndex);   // I'll wrap around the ring
        if(nodeToDeliverIndex == -1)
            return null;       // otherwise return closest next id
        else
            return nodes.get(nodeToDeliverIndex);
    }

    public void printNodeList() {
        for(NodeInfo ni: nodes)
            System.out.println("ID: " + ni.getId() + "\tIP: " + ni.getIp() + "\tport: " + ni.getPort());
    }

}
