package sdp.biwi.gateway;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import java.net.URI;

import sdp.biwi.node.ExitListener;

import java.lang.InterruptedException;


public class GatewayMain {

    public static final String SERVER_BASE_URI = "http://localhost:8080/pm10/";

    public static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig().packages("sdp/biwi/gateway/rest");
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(SERVER_BASE_URI), rc);
    }

    public static void main(String[] args) {

        HttpServer server = null;
        try {
            server = startServer();
        } catch(Exception e) {
            System.out.println("Something went wrong starting the server.");
        }
        Thread exitListener = new Thread(new ExitListener());
        exitListener.start();
        System.out.println("Press ENTER at any time to initiate exit procedure.");
        try {
            exitListener.join();
            assert server != null;
            server.shutdown();
        } catch(InterruptedException e) {
            System.out.println("Server interrupted.");
        } catch(NullPointerException e) {
            System.out.println("Couldn't stop unexisting server.");
        }
        System.out.println("Server clean shut down.");
        System.exit(0);
    }

}
