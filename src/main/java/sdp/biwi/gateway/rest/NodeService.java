package sdp.biwi.gateway.rest;

import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import sdp.biwi.utils.NodeList;
import sdp.biwi.node.NodeInfo;


@Path("node")
public class NodeService {

    private static final Gson gson = new Gson();

    @POST
    @Path("register")
    @Produces("application/json")
    @Consumes("application/json")
    public Response registerNode(String nodeInfoStr) {
        NodeInfo ni = gson.fromJson(nodeInfoStr, NodeInfo.class);
        System.out.println("New node " + ni.getId() + " trying to enter the network.");
        NodeList nodeList = NodeList.getInstance();
        NodeList listToReturn = nodeList.addNode(ni);
        if(listToReturn != null) {
            System.out.println("Node " + ni.getId() + " registered successfully.");
            return Response.status(200).entity(gson.toJson(listToReturn)).build();
        }
        System.out.println("Node ID " + ni.getId() + " already in use.");
        return Response.status(400).entity(null).build();
    }

    @POST
    @Path("exit")
    @Consumes("application/json")
    public Response exitFromNetwork(String nodeId) {
        int idToDelete = gson.fromJson(nodeId, Integer.class);
        NodeList nodeList = NodeList.getInstance();
        NodeList listToReturn = nodeList.removeNode(idToDelete);
        System.out.println("Node " + idToDelete + " exiting the network.");
        if(listToReturn != null) { // remove it from the net
            System.out.println("Node " + idToDelete + " deleted from node map.");
            return Response.status(200).entity(gson.toJson("removed " + idToDelete)).build();
        }
        return Response.status(404).entity(gson.toJson("Node ID not found.")).build();
    }


}
