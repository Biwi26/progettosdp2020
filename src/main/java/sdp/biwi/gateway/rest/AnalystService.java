package sdp.biwi.gateway.rest;

import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import java.util.List;
import java.util.ArrayList;

import sdp.biwi.utils.NodeList;
import sdp.biwi.gateway.StatsHistory;
import sdp.biwi.utils.DevMean;
import sdp.biwi.utils.StatMsg;



@Path("analyst")
public class AnalystService {

    private static final Gson gson = new Gson();
    private static NodeList nodeList;
    private static StatsHistory statsHistory;

    @GET
    @Path("nodes")
    @Produces("application/json")
    public Response getNodes() {
        System.out.println("Analyst queried node number.");
        nodeList = NodeList.getInstance();
        return Response.status(200).entity(gson.toJson(nodeList.getNodesNumber())).build();
    }

    @POST
    @Path("stats")
    @Consumes("application/json")
    @Produces("application/json")
    public Response readStats(String amountStr) {
        int amount = gson.fromJson(amountStr, Integer.class);
        System.out.println("Queried last " + amount + " stats.");
        statsHistory = StatsHistory.getInstance();
        List<StatMsg> subStats = statsHistory.readNStats(amount);
        return Response.status(200).entity(gson.toJson(new ArrayList<StatMsg>(subStats))).build();
    }

    @POST
    @Path("dev-mean")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getDevMeanFromStats(String amountStr) {
        int amount = gson.fromJson(amountStr, Integer.class);
        System.out.println("Analyst queried mean and std. deviation of last " + amount + " stats.");
        statsHistory = StatsHistory.getInstance();
        List<StatMsg> subStats = statsHistory.readNStats(amount);
        StatMsg mean = DevMean.calculateMean(subStats);
        double stdDev = DevMean.calculateStdDev(subStats);
        DevMean dm = new DevMean(stdDev, mean);
        return Response.status(200).entity(gson.toJson(dm)).build();
    }

}
